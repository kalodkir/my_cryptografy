package fel.cvut.cz;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.Stack;

/**
 * Created by stormnight on 8/17/17.
 */
public class Blockchain {
    private String initial_hash;
    private Stack<Block> blocks;

    public Blockchain(StartBlock block) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        this.blocks=new Stack<Block>();
        this.initial_hash=block.Get_hash();
        this.blocks.push(block);
    }

    public void Add_transaction(Transaction transaction) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        Block block = new TransactionBlock(transaction, blocks.lastElement().Get_hash());
        blocks.push(block);
    }

    public boolean Control() throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String hash=initial_hash;

        for(Enumeration<Block> eblock=blocks.elements(); eblock.hasMoreElements();){
            Block block=eblock.nextElement();
            if(!block.Get_base_hash().equals(hash)) return false;
            else hash=block.Get_hash();
        }

        return true;
    }

    public void Print(BufferedWriter writer) throws IOException {
        for(Enumeration<Block> eblock=blocks.elements(); eblock.hasMoreElements();){
            writer.write("--- Block---");
            writer.newLine();
            Block block=eblock.nextElement();
            block.Print(writer);
            writer.write("--- End block---");
            writer.newLine();
        }
    }
}
