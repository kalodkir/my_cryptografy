package fel.cvut.cz;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;


/**
 * Created by stormnight on 8/17/17.
 */
public abstract class Block {
    private String base_hash;

    public Block(String base_hash) {
        this.base_hash = base_hash;
    }

    public String Get_base_hash() {
        return base_hash;
    }

    public abstract String Get_hash() throws NoSuchAlgorithmException, UnsupportedEncodingException;

    public void Print(BufferedWriter writer) throws IOException {
        writer.write(base_hash);
        writer.newLine();
    }
}
