package fel.cvut.cz;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by stormnight on 8/17/17.
 */
public class StartBlock extends Block{
    public StartBlock(String base_hash){
        super(base_hash);
    }

    public String Get_hash() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return Get_base_hash();
    }
}
