package fel.cvut.cz;

import javax.xml.bind.DatatypeConverter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by stormnight on 8/17/17.
 */
public class TransactionBlock extends Block{
    private Transaction transaction;

    public TransactionBlock(Transaction transaction, String previous_hash){
        super(previous_hash);
        this.transaction=transaction;
    }

    public Transaction Get_transaction(){
        return transaction;
    }

    public String Get_hash() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digest=MessageDigest.getInstance("SHA-256");
        byte[] hash=digest.digest((transaction.toString() +
                super.Get_base_hash().toString()).getBytes("UTF-8"));

        return DatatypeConverter.printHexBinary(hash);
    }

    public void Print(BufferedWriter writer) throws IOException {
        super.Print(writer);
        transaction.Print(writer);
    }
}
