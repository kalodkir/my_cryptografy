package fel.cvut.cz;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Created by stormnight on 8/17/17.
 */
public class Transaction {
    private String sender;
    private String recipient;
    private int money;

    public Transaction(String sender, String recipient, int money){
        this.sender=sender;
        this.recipient=recipient;
        this.money=money;
    }

    public String Get_sender(){
        return sender;
    }

    public String Get_recipient(){
        return recipient;
    }

    public int Gen_money(){
        return money;
    }

    public String toString(){
        return sender+recipient+money;
    }

    public void Print(BufferedWriter writer) throws IOException {
        writer.write(sender);
        writer.newLine();
        writer.write(recipient);
        writer.newLine();
        writer.write(String.valueOf(money));
        writer.newLine();
    }
}
