//
// Created by stormnight on 8/12/17.
//

#include <iostream>
#include "RSA.h"
#include "numbers_functions.h"

mpz_t* power_mpz_array(const mpz_t* parray, const mpz_t& pow, const mpz_t& mod, unsigned int len);


RSA::RSA(mp_limb_t count_bits) {
    //initialization pseudorandom generator
    set_random_generator(randstate);

    mpz_t p;
    mpz_t q;
    mpz_t fn;
    mpz_t one;

    mpz_init(p);
    mpz_init(q);
    mpz_init(n);
    mpz_init(e);
    mpz_init(d);
    mpz_init(fn);
    mpz_init_set_d(one,1);

    generate_rsa_factor(p,randstate,count_bits/2);
    generate_rsa_factor(q,randstate,count_bits/2);

    //set rsa modul
    mpz_mul(n,p,q);

    //calculate fn
    mpz_sub(p,p,one);
    mpz_sub(q,q,one);
    mpz_mul(fn,p,q);

    //keys generation
    generate_public_and_private_keys(fn,e,d,randstate);

    //clear temporary variables
    mpz_clear(p);
    mpz_clear(q);
    mpz_clear(fn);
    mpz_clear(one);
}


RSA::RSA(const mpz_t &n, const mpz_t &d, const mpz_t &e) {
    mpz_init_set(this->n,n);
    mpz_init_set(this->d,d);
    mpz_init_set(this->e,e);
}


void RSA::Encription(const mpz_t* pmessage, const mpz_t* prandom_vector, mpz_t* pencrypted_message,
                       mpz_t* pencrypted_random, unsigned int len) const {
    //calculate encrypted random vector
    mpz_t* pencrypted_random_result = power_mpz_array(prandom_vector,this->e,this->n,len);

    //calculate sum pmessage and random vector
    for(unsigned int i=0; i<len; i++){
        mpz_add(pencrypted_message[i], pmessage[i],prandom_vector[i]);
        mpz_mod(pencrypted_message[i],pencrypted_message[i],n);
    }

    //calculate encrypted sum pmessage and random vector
    mpz_t* pencrypted_message_result = power_mpz_array(pencrypted_message,this->e,this->n,len);

    //copy results
    for(unsigned int i=0; i<len; i++){
        mpz_init_set(pencrypted_message[i],pencrypted_message_result[i]);
        mpz_init_set(pencrypted_random[i],pencrypted_random_result[i]);
    }

    clear_mzp_array(pencrypted_random_result,len);
    clear_mzp_array(pencrypted_message_result,len);
    delete[] pencrypted_random_result;
    delete[] pencrypted_message_result;
}


mpz_t* RSA::Decription(const mpz_t *pmessage_encrypted, const mpz_t* p_random_vector_encrypted, unsigned int len) const {
    mpz_t* pmessage_add_random = power_mpz_array(pmessage_encrypted,this->d,this->n,len);
    mpz_t* prandom_vector = power_mpz_array(p_random_vector_encrypted,this->d,this->n,len);

    mpz_t inverse;

    mpz_init(inverse);

    mpz_t* pmessage = new mpz_t[len];
    for(unsigned int i=0; i<len; i++){
        mpz_init(pmessage[i]);
        mpz_sub(inverse,n,prandom_vector[i]);
        mpz_add(pmessage[i],pmessage_add_random[i],inverse);
        mpz_mod(pmessage[i],pmessage[i],n);
    }

    mpz_clear(inverse);
    clear_mzp_array(pmessage_add_random,len);
    clear_mzp_array(prandom_vector,len);
    delete[] pmessage_add_random;
    delete[] prandom_vector;
    return pmessage;
}


bool RSA::Control() const {
    mpz_t fn;
    mpz_t one;
    mpz_t result;
    bool operation_result;

    mpz_init(fn);
    mpz_init(result);
    mpz_init_set_d(one,1);

    mpz_sub(fn,n,one);
    mpz_mul(result,e,d);
    mpz_mod(result,result,fn);

    if(mpz_cmp(result,one)==0) operation_result = true;
    else operation_result= false;

    //clear
    mpz_clear(fn);
    mpz_clear(one);
    mpz_clear(result);

    return result;
}


std::ostream& operator<<(std::ostream& os, const RSA& lrsa){
    char* pnumber_buffer= nullptr;

    pnumber_buffer=mpz_get_str(pnumber_buffer,10,lrsa.n);
    os<<"Module: "<<pnumber_buffer<<std::endl;
    delete[] pnumber_buffer;
    pnumber_buffer= nullptr;

    pnumber_buffer=mpz_get_str(pnumber_buffer,10,lrsa.e);
    os<<"Private key: "<<pnumber_buffer<<std::endl;
    delete[] pnumber_buffer;
    pnumber_buffer= nullptr;

    pnumber_buffer=mpz_get_str(pnumber_buffer,10,lrsa.d);
    os<<"Public key: "<<pnumber_buffer<<std::endl;
    delete[] pnumber_buffer;
    return os;
}


RSA::~RSA() {
    mpz_clear(n);
    mpz_clear(e);
    mpz_clear(d);
}


mpz_t* convert_message_to_mpz_array(const char* pmessage, unsigned int len){
    mpz_t* presult=new mpz_t[len];

    //copy char to mpz_t
    for(unsigned int i=0; i<len; i++)
        mpz_init_set_d(presult[i],pmessage[i]);

    return presult;
}


mpz_t* RSA::Generate_random_vector(const mpz_t* pmessage, unsigned int len){
    mpz_t* prandom_vector=new mpz_t[len];

    mpz_t gcd_random;
    mpz_t gcd_pmessage;
    mpz_t message_add_random;

    mpz_init(gcd_random);
    mpz_init(gcd_pmessage);
    mpz_init(message_add_random);


    for(unsigned int i=0; i<len; i++){
        mpz_init(prandom_vector[i]);
        do {
            mpz_urandomm(prandom_vector[i], randstate, n);
            mpz_add(message_add_random,pmessage[i],prandom_vector[i]);

            mpz_gcd(gcd_random,n,prandom_vector[i]);
            mpz_gcd(gcd_pmessage, n, message_add_random);
        }while((mpz_cmp_d(gcd_random,1)!=0)&&(mpz_cmp_d(message_add_random,1)!=0));
        //gmp_printf("%Zd\n",prandom_vector[i]);
    }

    mpz_clear(gcd_random);
    mpz_clear(gcd_pmessage);
    mpz_clear(message_add_random);

    return prandom_vector;
}


char* convert_message_to_char_array(const mpz_t* pmessage, unsigned int len){
    char* presult=new char[len+1];

    for(unsigned int i=0; i<len; i++)
        presult[i]=mpz_get_ui(pmessage[i]);

    presult[len]='\0';

    return presult;
}


void print_mpz_array(const mpz_t* parray, std::ostream& os, unsigned int len){
    char* pnumber_buffer= nullptr;

    for(unsigned int i=0; i<len; i++){
        pnumber_buffer=mpz_get_str(pnumber_buffer,10,parray[i]);
        os<<'|'<<pnumber_buffer<<'|';
        delete[] pnumber_buffer;
        pnumber_buffer= nullptr;
    }

    os<<std::endl;
}


void clear_mzp_array(mpz_t* parray, unsigned int len){
    for(unsigned int i=0; i<len; i++)
        mpz_clear(parray[i]);
}


void init_mpz_array(mpz_t* parray, unsigned int len){
    for(unsigned int i=0; i<len; i++)
        mpz_init(parray[i]);
}


mpz_t* power_mpz_array(const mpz_t* parray, const mpz_t& pow, const mpz_t& mod, unsigned int len){
    mpz_t* presult=new mpz_t[len];

    for(unsigned int i=0; i<len; i++){
        mpz_init(presult[i]);
        mpz_powm_sec(presult[i], parray[i], pow, mod);
    }

    return presult;
}