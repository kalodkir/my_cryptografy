//
// Created by stormnight on 8/12/17.
//
#include <gmp.h>
#include <ostream>

#ifndef RSA_H
#define RSA_H


class RSA{
private:
    mpz_t n;    //RSA module
    mpz_t d;    //private key
    mpz_t e;    //public key

    gmp_randstate_t randstate;
public:
    RSA(mp_limb_t count_bits);
    RSA(const mpz_t& n, const mpz_t& d, const mpz_t& e);

    void Get_module(mpz_t& n) const { mpz_init_set(n,this->n); }
    void Get_private_key(mpz_t& d) const { mpz_init_set(d,this->d); }
    void Get_public_key(mpz_t& e) const { mpz_init_set(e,this->e); }

    void Encription(const mpz_t* pmessage, const mpz_t* random_vector, mpz_t* pencrypted_message,
                    mpz_t* pencrypted_random, unsigned int len) const;
    mpz_t* Decription(const mpz_t* pmessage, const mpz_t* random_vector, unsigned int len) const;
    mpz_t* Generate_random_vector(const mpz_t* pmessage, unsigned int len);

    bool Control() const;
    ~RSA();

    friend std::ostream& operator<<(std::ostream& os, const RSA& lrsa);
};


mpz_t* convert_message_to_mpz_array(const char* pmessage, unsigned int len);
char* convert_message_to_char_array(const mpz_t* pmessage, unsigned int len);
void print_mpz_array(const mpz_t* parray, std::ostream& os, unsigned int len);
void init_mpz_array(mpz_t* parray, unsigned int len);
void clear_mzp_array(mpz_t* parray, unsigned int len);

#endif