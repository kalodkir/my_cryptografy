//
// Created by stormnight on 8/12/17.
//

#ifndef MY_PGP_NUMBERS_FUNCTIONS_H
#define MY_PGP_NUMBERS_FUNCTIONS_H

#include <gmp.h>

void set_random_generator(gmp_randstate_t& randstate);
void generate_rsa_factor(mpz_t& number, gmp_randstate_t& randstate, mp_bitcnt_t count_bits);
void generate_public_and_private_keys(mpz_t& fn, mpz_t& e, mpz_t& d, gmp_randstate_t& randstate);

#endif //MY_PGP_NUMBERS_FUNCTIONS_H