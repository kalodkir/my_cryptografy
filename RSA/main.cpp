#include <iostream>
#include <fstream>
#include "RSA.h"

void read_message(std::fstream& in, std::string& message);

int main(int argc, char* argv[]) {
    std::fstream in;
    std::fstream out;
    mp_limb_t rsa_module;

    in.open(argv[1], std::fstream::in);
    out.open(argv[2],std::fstream::out);

    in>>rsa_module;
    in.get();

    RSA rsa = RSA(rsa_module);
    out<<rsa;
    out<<"Control RSA: "<<rsa.Control()<<std::endl;

    std::string message;
    read_message(in,message);

    mpz_t* pmessage=convert_message_to_mpz_array(message.c_str(),message.length());
    out<<"Open text: ";
    print_mpz_array(pmessage, out, message.length());

    mpz_t* prandom_vector=rsa.Generate_random_vector(pmessage,message.length());
    out<<"Random vector: ";
    print_mpz_array(prandom_vector, out, message.length());

    mpz_t* pmessage_e=new mpz_t[message.length()];
    mpz_t* prandom_e=new mpz_t[message.length()];

    init_mpz_array(pmessage_e,message.length());
    init_mpz_array(prandom_e,message.length());

    rsa.Encription(pmessage,prandom_vector,pmessage_e,prandom_e,message.length());
    out<<"Encrypted text: ";
    print_mpz_array(pmessage_e, out, message.length());

    mpz_t* pmessage_d=rsa.Decription(pmessage_e,prandom_e,message.length());
    out<<"Decrypted message: ";
    print_mpz_array(pmessage_d,out,message.length());

    char* pot=convert_message_to_char_array(pmessage_d,message.length());
    out<<"Open decrypted text: "<<pot<<std::endl;

    clear_mzp_array(pmessage,message.length());
    clear_mzp_array(prandom_vector,message.length());
    clear_mzp_array(pmessage_e,message.length());
    clear_mzp_array(prandom_e,message.length());
    clear_mzp_array(pmessage_d,message.length());

    delete[] pmessage;
    delete[] prandom_vector;
    delete[] pmessage_e;
    delete[] prandom_e;
    delete[] pmessage_d;
    delete[] pot;

    return 0;
}


void read_message(std::fstream& in, std::string& message){
    std::string temp;
    while(in){
        std::getline(in,temp);
        message+=temp+"\n";
    }
}