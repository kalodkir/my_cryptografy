package fel.cvut.cz;

import java.io.*;
import java.security.NoSuchAlgorithmException;

public class Main {

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        BufferedReader reader=new BufferedReader(new FileReader(args[0]));
        BufferedWriter writer=new BufferedWriter(new FileWriter(args[1]));

        String sender;
        String recipient;
        int money;
        String hash;
        hash=reader.readLine();

        StartBlock startBlock=new StartBlock(hash);
        Blockchain blockchain=new Blockchain(startBlock);

        while ((sender=reader.readLine())!=null){
            recipient=reader.readLine();
            money=new Integer(reader.readLine());
            Transaction transaction=new Transaction(sender, recipient, money);
            blockchain.Add_transaction(transaction);
        }

        System.out.println("Control transaction: " + (blockchain.Control()?"Passed":"Not passed"));
        blockchain.Print(writer);
        writer.close();
    }
}
