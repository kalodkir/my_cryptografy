//
// Created by stormnight on 8/12/17.
//

#include "numbers_functions.h"
#include <time.h>
#include <iostream>

const unsigned int key_border=16;


void generate_rsa_factor(mpz_t& key, gmp_randstate_t& randstate, mp_bitcnt_t count_bits){
    mpz_t minimal_number;
    mpz_t maximal_number;
    mpz_t interval;
    mpz_t random_value;

    //initialization
    mpz_init_set_d(minimal_number,2);
    mpz_init_set_d(maximal_number,2);
    mpz_init(interval);
    mpz_init(random_value);

    //set border
    mpz_pow_ui(minimal_number,minimal_number,count_bits);
    mpz_pow_ui(maximal_number,maximal_number,count_bits + 1);

    //calculate interval (max - min)
    mpz_sub(interval,maximal_number,minimal_number);

    int prime_test_result;

    //generate prime number
    unsigned int count_cycles = 0;
    do{
        mpz_urandomm(random_value,randstate,interval);
        mpz_add(key,minimal_number,random_value);
        prime_test_result = mpz_probab_prime_p(key,40);
        count_cycles++;
    }while (prime_test_result==0);
    std::cout<<"Count cycles for factor: "<<count_cycles<<std::endl;

    //clear
    mpz_clear(maximal_number);
    mpz_clear(minimal_number);
    mpz_clear(interval);
}


void generate_public_and_private_keys(mpz_t& fn, mpz_t& e, mpz_t& d, gmp_randstate_t& randstate){
    mpz_t min_key;
    mpz_t interval;
    mpz_t key;
    mpz_t gcd;
    mpz_t one;

    //initialization
    mpz_init_set_d(min_key,2);
    mpz_init_set_d(one,1);
    mpz_init(key);
    mpz_init(interval);
    mpz_init(gcd);

    //set low border
    mpz_pow_ui(min_key,min_key,key_border);

    //calculation interval
    mpz_sub(interval,fn,min_key);

    //calculate keys
    unsigned int count_cycles=0;
    do{
        mpz_urandomm(key,randstate,interval);
        mpz_add(e,key,min_key);
        mpz_gcd(gcd,e,fn);

        count_cycles++;

        //key control and calculate pair key
        if(mpz_cmp(one,gcd)==0) {
            mpz_invert(d, e, fn);}
        else continue;

    }while ((mpz_cmp(e,min_key)<0)||(mpz_cmp(d,min_key)<0));
    std::cout<<"Count cycles for keys: "<<count_cycles<<std::endl;

    //clear
    mpz_clear(min_key);
    mpz_clear(interval);
    mpz_clear(key);
    mpz_clear(gcd);
    mpz_clear(one);
}


void set_random_generator(gmp_randstate_t& randstate){
    gmp_randinit_mt(randstate);
    unsigned long int seed = (unsigned long int)time(NULL);
    gmp_randseed_ui(randstate,seed);
}